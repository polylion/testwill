%mettre le debug dans test will poly a: strcmp(parametres.SourceEntiere,'oui')
% ceci est un programme quitrace les courbes d'interpolations de phi et
% theta en foonction de mu70, et aui seront utilisees pour le calcul du
% critere par la suite. 



close all

subplot(2,2,1)



plot(cmp.compton(:,1), cmp.compton(:,2), 'ko', cmp.compton(:,1), cmp.compton(:,3), 'k+',cmp.photo(:,1), cmp.photo(:,2), 'kd', cmp.photo(:,1), cmp.photo(:,3), 'kx' );

  
 hold on 
plot((0:0.01:1.4)',poly.attmodel.photo((0:0.01:1.4)'))
hold on
plot((0:0.01:1.4)',poly.attmodel.compton((0:0.01:1.4)'),'-.')

xlim([0,1.4]);
title('\phi et \theta selon l''att�nuation mu � 70 kev d''un mat�riau')
xlabel('\mu(70 keV) en cm^{-1}');
ylabel('cm^{-1}');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%55

subplot(2,2,2)

plot(cmp.compton(:,1), cmp.compton(:,2), 'ko', cmp.compton(:,1), cmp.compton(:,3), 'k+',cmp.photo(:,1), cmp.photo(:,2), 'kd', cmp.photo(:,1), cmp.photo(:,3), 'kx' );

  
 hold on 
plot((0:0.01:10)',poly.attmodel.photo((0:0.01:10)'))
hold on
plot((0:0.01:10)',poly.attmodel.compton((0:0.01:10)'),'-.')
;
title('\phi et \theta selon l''att�nuation mu � 70 kev d''un mat�riau')

xlim([0,10]);

legend('\phi r�el de plusieurs mat�riaux en fonction du \mu_{70} de ces mat�riaux',...
'\phi calcul� pour ces mat�riaux par la fonction d''interpolation',...
'\theta r�el de plusieurs mat�riaux en fonction du \mu_{70} de ces mat�riaux',...
'\theta calcul� pour ces mat�riaux par la fonction d''interpolation',...
'courbe d''interpolation pour l''att�nuation photo�lectrique \phi',...
'courbe d''interpolation pour l''att�nuation de Compton \theta')
xlabel('\mu(70 keV) en cm^{-1}');
ylabel('cm^{-1}');

