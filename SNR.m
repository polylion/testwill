function snr = SNR(SinoAvecBruit,SinoSansBruit)
ValB = recupVal(SinoAvecBruit);
ValSB = recupVal(SinoSansBruit);
snr = (ValSB(:))'*(ValSB(:))/ ((ValSB(:)-ValB(:))'*(ValSB(:)-ValB(:)));
snr = 10*log10(snr);






