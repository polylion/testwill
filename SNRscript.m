

% Espace de test
% Remarque: tant que je change juste les valeurs �a va mais si je veux
% metter d'autres param�tres il faut que je fasse attention � voir que mes
% scripts sont bien adapt�s
addpath(genpath('/export/tmp/wdevazel'));
parametres = struct;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parametres importants
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% rajouter ici les parametres d une lesion si besoin
parametres.Geom = 'Cartesien';
parametres.nomFant = 'fantomebruit'; % mettre le nom d'un fichier parametrant les fantomes, present dans le dossier 
% '/export/temp/wdevazel/testWill/Generation/GenerationSimple/DossierSinos' 
parametres.CritereAdeq = 'Exponential_Poisson';
parametres.CriterePenal = 'PenalGradObj_L2';
parametres.lambda =0.001;     
parametres.delta = 5e-03;
parametres.precond = 'Identite';
parametres.max_iter = 100000;
parametres.epsilon = 1e-10;
parametres.ImageInitiale = 'non'; % mettre fbp si c est la fbp (on peut mettre posFBP aussi)
parametres.lowb = -Inf; % attention ici ca veut dire  contrainte de positivite
parametres.imageInitialeVal = 0; % pas pris en compte si FBP ou FBPpos

% Param�tres globaux de la reconstruction
parametres.nRayons  = 13;
parametres.nRayonsz = 1;
parametres.nTranRec = 1;% nombre de tranches � reconstruire

% parametres de la source

parametres.SourceEntiere = 'non';
parametres.nb_ge_samples = 20;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Execution et sauvegarde selon les r�gles definies dans les autres
% programmes
run(parametres.nomFant)











Fant = Sino.Fant;
GeoP = Sino.GeoP;
TablePosition= Sino.TablePosition;

mAs = Sino.ParP.mAs*nRth*nRz;



ParP = ParP_XCAT(containers.Map({'mAs', 'source','Poisson'},{mAs, struct('Fichier', ficsource),logical(0)}));

nomSino = 'SinoSansBruit0p0043bigAlu120ray';
dir2 = strcat('/export/tmp/wdevazel/testWill/Generation/GenerationSimple/DossierSinos/',nomSino);                      % r�pertoire pour le stockage du sinogramme

MapSino = containers.Map({'Fant', 'GeoP', 'ParP', 'TablePosition', 'RefAngleDeg'},{Fant, GeoP, ParP,TablePosition, 90});
SinoSansBruit = Sino_Factory.create('XCAT', 'Map', MapSino,'dir', dir2,'nRth',nRth,'nRz',nRz);

snr = SNR(Sino,SinoSansBruit)
