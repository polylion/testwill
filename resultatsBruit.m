function results = resultatsBruit(mAs, Ech, tranch, Ray,nomdoss)

results = [];
compteur = 0;
for i = 1:length(mAs)
    for j = 1:length(Ech)
        for k = 1:length(tranch)
            for l = 1:length(Ray)
                addpath(genpath('/export/tmp/wdevazel'));
                compteur = compteur +1;
                % Espace de test
                % Remarque: tant que je change juste les valeurs �a va mais si je veux
                % metter d'autres param�tres il faut que je fasse attention � voir que mes
                % scripts sont bien adapt�s
                parametres = struct;
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % Parametres importants
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                
                
                
                % Ceci est un fichier pour rentrer facilement les parametres des fantomes a
                % reconstruire, avec les lesions
                % ce fichier est a copier, a modifier, et a coller dans le bon dossier
                
                
                % Tranche Initiale et tranche finale : si on veut uniquement des lesions,
                % faire autour de z = 140 je crois
                
                parametres.factMisEch = Ech(j)   ;
                parametres.Bras              =   0   ;        % pr�sence ou non d'un bras
                parametres.epaisseurTranches =   1   ;        % mm
                parametres.trancheIniReg        =   tranch(k) ;     % tranches qu'on voudra reconstruire
                parametres.trancheFinReg        =   tranch(k)  ;
                parametres.TrancheUnique='oui';
                
                
                parametres.nRth = Ray(l);
                parametres.nRz = 1;
                
                parametres.Poisson = 1; % 1 pour rajouter du bruit de Poisson
                parametres.mAs = mAs(i) ;
                
                % precision si oui ou non il y a des lesions
                
                parametres.Lesion = 'non';
                
                if tranch(k) > 2000
                    parametres.Lesion = 'oui';
                    % precision si oui ou non il y a des lesions
                    
                    
                    
                    % creation de l eau comme milieu
                    
                    
                    z  = 3000;
                    materials = 15; % brain ca doit etre mou
                    proportions = 1;
                    
                    diam = 400;
                    
                    x = 0;
                    y = 0;
                    Lesion5 = LesionXCAT(containers.Map({'x','y','z','diam','materials','proportions'},...
                        {x,y,z,diam,materials,proportions}));
                    
                    %Creation des lesions metalliques
                    
                    z  = 3000;
                    materials = 20; % aluminium
                    proportions = 1;
                    
                    %Creation des grosses lesions metalliques
                    
                    diam = 60;
                    
                    x = -75;
                    y = 75;
                    Lesion1 = LesionXCAT(containers.Map({'x','y','z','diam','materials','proportions'},...
                        {x,y,z,diam,materials,proportions}));
                    
                    
                    y = -75;
                    x=75;
                    Lesion2 = LesionXCAT(containers.Map({'x','y','z','diam','materials','proportions'},...
                        {x,y,z,diam,materials,proportions}));
                    
                    %Creation des petites lesions de titane
                    
                    materials = 21;
                    diam = 20;
                    
                    x = 75;
                    y = 75;
                    Lesion3 = LesionXCAT(containers.Map({'x','y','z','diam','materials','proportions'},...
                        {x,y,z,diam,materials,proportions}));
                    
                    x = -75;
                    y = -75;
                    Lesion4 = LesionXCAT(containers.Map({'x','y','z','diam','materials','proportions'},...
                        {x,y,z,diam,materials,proportions}));
                    
                    
                    
                    %%%%%%%%%%%%%%%%%%%%
                    Lesions = [Lesion5 Lesion1 Lesion2 Lesion3 Lesion4];
                    
                    
                end
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    % rajouter ici les parametres d une lesion si besoin
                    parametres.Geom = 'Cartesien';
                    parametres.nomFant = strcat('DossFantomeBruit',nomdoss,'/SinoacBruit',nomdoss,num2str(compteur)); % mettre le nom d'un fichier parametrant les fantomes, present dans le dossier
                    % '/export/temp/wdevazel/testWill/Generation/GenerationSimple/DossierSinos'
                    
                    
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %% Execution et sauvegarde selon les r�gles definies dans les autres
                    % programmes
                    
                    names = fieldnames(parametres);
                    
                    for p = 1:length(names)
                        key = char(names(p));
                        eval(strcat(key,' = parametres.(key)') );
                    end
                    
                    GenFantEtSino % j ai du mettre parametres pour recuperer les tanches ini et reg pour le trace aes
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    Fant = Sino.Fant;
                    GeoP = Sino.GeoP;
                    TablePosition= Sino.TablePosition;
                    
                    
                    
                    ParP = ParP_XCAT(containers.Map({'mAs', 'source','Poisson'},{mAs, struct('Fichier', ficsource),logical(0)}));
                    
                    dir2 = strcat('/export/tmp/wdevazel/testWill/Generation/GenerationSimple/DossierSinos/DossFantomeBruit',nomdoss,'/SinoacBruit',nomdoss,num2str(compteur));                      % r�pertoire pour le stockage du sinogramme
                    
                    MapSino = containers.Map({'Fant', 'GeoP', 'ParP', 'TablePosition', 'RefAngleDeg'},{Fant, GeoP, ParP,TablePosition, 90});
                    
                    SinoSansBruit = Sino_Factory.create('XCAT', 'Map', MapSino,'dir', dir2,'nRth',nRth,'nRz',nRz);
                    
                    snr = SNR(Sino,SinoSansBruit);
                    
                    
                    line = [mAs(i) Ech(j) tranch(k) Ray(l) snr];
                    results = [results ; line ];
            end 
        end 
    end 
end
csvwrite('~/Desktop/transfert/resultatsBruit.csv',results)