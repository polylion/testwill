
% Espace de test
% Remarque: tant que je change juste les valeurs �a va mais si je veux
% metter d'autres param�tres il faut que je fasse attention � voir que mes
% scripts sont bien adapt�s
addpath(genpath('/export/tmp/wdevazel'));
parametres = struct;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parametres importants
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% rajouter ici les parametres d une lesion si besoin
parametres.Geom = 'Cartesien';
parametres.nomFant = 'fantomebruit'; % mettre le nom d'un fichier parametrant les fantomes, present dans le dossier 
% '/export/temp/wdevazel/testWill/Generation/GenerationSimple/DossierSinos' 
parametres.CritereAdeq = 'polychromAdeqPoisson';
parametres.CriterePenal = 'PenalGradObj_L2';
parametres.lambda =0.001;     
parametres.delta = 5e-03;
parametres.precond = 'Identite';
parametres.max_iter = 100000;
parametres.epsilon = 1e-10;
parametres.ImageInitiale = 'non'; % mettre fbp si c est la fbp (on peut mettre posFBP aussi)
parametres.lowb = -Inf; % attention ici ca veut dire  contrainte de positivite
parametres.imageInitialeVal = 0; % pas pris en compte si FBP ou FBPpos

% Param�tres globaux de la reconstruction
parametres.nRayons  = 13;
parametres.nRayonsz = 1;
parametres.nTranRec = 1;% nombre de tranches � reconstruire

% parametres de la source

parametres.SourceEntiere = 'non';
parametres.nb_ge_samples = 20;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Execution et sauvegarde selon les r�gles definies dans les autres
% programmes
run(parametres.nomFant)

lancement % selectionner la bonne generation (dans scripts)
% j ai change ce programme pour mettre la contrainte de positivite
% dans lancement il y a le directory des resultats
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Affichage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% On peut rajouter la colormap (ici on met juste le vecteure des Slices
% 


% %specification de la tranche a tracer... doit etre compris entre
% %trancheIniReg et TrancheFinReg  ( Aller voir dans parametres secondaires)
% trancheTraceIni = 901;
% trancheTraceFin = 901;
% 
% Colormap = [-0.05 0.4];
% 
% load(strcat(directory,name));
% afficheSimple(Sriefinal, parametres, trancheTraceIni, trancheTraceFin ,'Colormap',Colormap); % si je veux je peux ausi mettre la colormap


