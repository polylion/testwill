function plotModifieCart(SieC, varargin)
%plot - RED�FINITION DE LA FONCTION PLOT POUR UN OBJET "Srie_Cart" 
%   Cette m�thode red�finit la fonction plot pour repr�senter
%   graphiquement tout ou partie d'une s�rie d'images (volume 3D) contenue
%   dans un objet de type "Srie_Cart" (s�rie en coordonn�es cart�siennes)
%
%   Formes d'appel : plot(SieC, [cl�, valeur, ...])
%                    SieC.plot([cl�, valeur, ...])
%   SieC : objet de la classe "Srie_Cart"
%   couples (cl�, valeur) : nom d'une caract�ristique de la s�rie, et
%                           valeur correspondante. Ceci permet de ne
%                           repr�senter qu'une partie des donn�es
%                           contenues dans la s�rie. 
%     Caract�ristique qu'il est possible de sp�cifier 
%       Slices : vecteur contenant les indices des tranches � tracer
%
%     Si une caract�ristique n'est pas sp�cifi�e, toutes les donn�es
%     disponibles sont trac�es.

%% R�cup�ration des param�tres et tests

nVargs = length(varargin);

if rem(nVargs, 2)
    error(['Param�tres d''entr�e invalide\n', ...
        'Couples cl�s-valeurs incomplets'], [])
end

MapDef = containers.Map({'Slices'}, ...
    {(1:SieC.GeoS.Slices)});

Map = containers.Map();
for iPar = 1:2:nVargs
    if ~ ischar(varargin{iPar})
        delete(MapDef)
        delete(Map)
        error(['Param�tre d''entr�e invalide\n', ...
            'Les cl�s doivent �tre des cha�nes de caract�res'], [])
    end
    Map(varargin{iPar}) = varargin{iPar+1};
end
MapComp = [MapDef; Map];
delete(Map)
delete(MapDef)

%% Avertissement si trac�s trop nombreux
if length(MapComp('Slices')) > 20
    disp(['Hum, plus de 20 tranches � tracer. ', ...
        'Les figures risquent d''�tre illisibles'])
    rep = input('Voulez-vous vraiment continuer [N / o] ? ', 's');
    if ~ strcmpi(rep, 'o')
        return
    end
end

%% Pr�paration des trac�s
idxSl = reshape(MapComp('Slices'), 1, []);
indicateur = 0;
if isKey(MapComp,'Colormap')
    indicateur = 1;
    vec = MapComp('Colormap');
end
delete(MapComp)
nSl = length(idxSl);
nCol = ceil(sqrt(nSl));
nLig = ceil(nSl / nCol);

minVal = min(reshape(SieC.Val(:, :, idxSl), [], 1));
maxVal = max(reshape(SieC.Val(:, :, idxSl), [], 1));

%% Trac�s proprement dits
figure('Name', SieC.SeriesDescription)
k = 1;
for iSl = idxSl
    subplot(nLig, nCol, k)
    
    if indicateur ==1 
        imagesc(SieC.GeoS.x, SieC.GeoS.y, SieC.Val(:, :, iSl), vec)
    else
        imagesc(SieC.GeoS.x, SieC.GeoS.y, SieC.Val(:, :, iSl), [...
        minVal, maxVal])
    end
    colormap gray, hdl = colorbar; axis image
    xlabel('x (mm)')
    ylabel('y (mm)')
    ylabel(hdl, SieC.GPhy.Label)        
    title(sprintf('Tranche %1.0f, z = %5.3f mm', ...
        iSl, SieC.GeoS.zSlices(iSl)), 'FontSize', 14)
    k = k + 1;
end

end

