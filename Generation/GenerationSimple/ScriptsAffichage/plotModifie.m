function plotModifie(Srie, varargin)
%plot - RED�FINITION DE LA FONCTION PLOT POUR UN OBJET "Series" 
%   Cette m�thode red�finit la fonction plot pour repr�senter
%   graphiquement tout ou partie d'une s�rie d'images (volume 3D) contenue
%   dans un objet de type "Series"
%
%   Formes d'appel : plot(Srie, cl�, valeur, ...)
%                    Srie.plot(cl�, valeur, ...)
%   Sino : objet de la classe "Series"
%   couples (cl�, valeur) : nom d'une caract�ristique de la s�rie, et
%                           valeur correspondante. Ceci permet de ne
%                           repr�senter qu'une partie des donn�es
%                           contenues dans la s�rie. 
%     Caract�ristique qu'il est possible de sp�cifier 
%       Scans : vecteur contenant les indices des scans � tracer
%       Slices : vecteur contenant les indices des tranches � tracer
%       Colormap: vecteur de deux elements qui contient les valeurs min et
%       max de la colormap choisie
%
%     Si une caract�ristique n'est pas sp�cifi�e, toutes les donn�es
%     disponibles sont trac�es.

%% R�cup�ration des param�tres et tests

nVargs = length(varargin);

if rem(nVargs, 2)
    error(['Param�tres d''entr�e invalide\n', ...
        'Couples cl�s-valeurs incomplets'], [])
end

MapDef = containers.Map({'Slices'}, ...
    {(1:Srie.Slices)});

Map = containers.Map();
for iPar = 1:2:nVargs
    if ~ ischar(varargin{iPar})
        delete(MapDef)
        delete(Map)
        error(['Param�tre d''entr�e invalide\n', ...
            'Les cl�s doivent �tre des cha�nes de caract�res'], [])
    end
    Map(varargin{iPar}) = varargin{iPar+1};
end
MapComp = [MapDef; Map];
delete(Map)
delete(MapDef)

if length(MapComp) > 1
    delete(MapComp)
    error('Cl�s invalides')
end

if ~ (prod(MapComp('Slices') >= 1) && ...
        prod(MapComp('Slices') <= Srie.Slices))
    delete(MapComp)
    error(['Indices des tranches � tracer invalides\n', ...
        'Doivent prendre des valeurs comprises entre 1 et %1.0f'], ...
        Srie.Slices)
end

%% Avertissement si trac�s trop nombreux
if length(MapComp('Slices')) > 20
    disp(['Hum, plus de 20 tranches � tracer. ', ...
        'Les figures risquent d''�tre illisibles'])
    rep = input('Voulez-vous vraiment continuer [N / o] ? ', 's');
    if ~ strcmpi(rep, 'o')
        return
    end
end

%% Pr�paration des trac�s
idxSl = reshape(MapComp('Slices'), 1, []);
delete(MapComp)
nSl = length(idxSl);
nCol = ceil(sqrt(nSl));
nLig = ceil(nSl / nCol);

minVal = min(reshape(Srie.Val(:, :, idxSl), [], 1));
maxVal = max(reshape(Srie.Val(:, :, idxSl), [], 1));

%% Trac�s proprement dits
figure('Name', Srie.SeriesDescription)
k = 1;
for iSl = idxSl
    subplot(nLig, nCol, k)
    if isKey(Map,'Colormap') 
        imagesc(Srie.x, Srie.y, Srie.Val(:, :, iSl), Map('ColorMap'))
    else
        imagesc(Srie.x, Srie.y, Srie.Val(:, :, iSl), [minVal, maxVal])
    end
    colormap gray, colorbar, axis image
    title(sprintf('Tranche no %1.0f, z = %5.3f mm', ...
        iSl, Srie.zSlices(iSl)), 'FontSize', 14)
    k = k + 1;
end

end

