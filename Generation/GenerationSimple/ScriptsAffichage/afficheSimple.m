function afficheSimple(Sriefinal, parametres, trancheTraceIni,trancheTraceFin, varargin)


% Cette fonction permet l'affichage des images de la serie prealablement
% calculee

% varargin peut contenir 
% Colormap : voulue, et le vecteur des 


Slices = trancheTraceIni-parametres.trancheIni+1:trancheTraceFin-parametres.trancheIni+1;

f = Sriefinal;
if strcmp(parametres.Geom,'Polaire')
    nbPixels = floor(32*parametres.factMisEch);
    f = Sriefinal.cvCart(nbPixels,nbPixels);
end


plotModifieCart(f,'Slices',Slices,varargin{1:length(varargin)}); % a modifier et finir pour que ca tienne compte de la colormap

end
