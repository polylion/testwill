
%Génération du sinogramme

if strcmp(Sino.Type,'RaySum') && (strcmp(parametres.CritereAdeq,'Exponential_Poisson') || strcmp(parametres.CritereAdeq,'polychromAdeqPoisson') || strcmp(parametres.CritereAdeq,'polAdPoissSimp'))  
    Sino = Sino.cvRPCount();
elseif strcmp(Sino.Type,'RPCount') && (strcmp(parametres.CritereAdeq,'Linear_Gauss_noPond') || strcmp(parametres.CritereAdeq, 'polychromAdeq') )
    Sino = Sino.cvRaySum();
end

    
    
sinoW = Sino;



dir = strcat('/export/tmp/wdevazel/testWill/Generation/GenerationSimple/DossierSinos/',parametres.nomFant);                      % répertoire pour le stockage du sinogramme


serieW = Sino.Fant.creationSerie(dir);



% a changer faire en sorte que sinoW et serieW soient la serie que l on
% veut reconstruire dans le sens ou ca prendra en compte uniquement les
% bonnes tranches ( par exemple une seule tranche)
