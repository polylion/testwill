% GeomCyclCreation

%% Conversion en coordonnées cylindriques
nbPixels = Sino.Fant.nbPixels; %attention: il doit y en avoir autant que pour la génération du sinogramme
Thetas = Sino.GeoP.nProjections;
Rhos = round(nbPixels^2 / Thetas);
serieW = serieWC.cvPol(Rhos, Thetas);
