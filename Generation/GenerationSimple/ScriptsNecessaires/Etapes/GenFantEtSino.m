%% Param�tres g�n�raux � r�gler pour la simulation

ROOT = fileparts(fileparts(which('testSinoXCAT.m'))); %fichier dans lequel se trouvent les objets XCAT (doit correspondre � "XCAT")                      % ajout du dossier contenant tous mes r�pertoires


dir = strcat('/export/tmp/wdevazel/testWill/Generation/GenerationSimple/DossierSinos/',parametres.nomFant);                      % r�pertoire pour le stockage du sinogramme




%% G�n�ration d'un fant�me


%% Caract�ristiques du fant�me: � r�gler !



tailleTranches = 400; % mm : doit �tre fixe je crois





%% Patient
Pat = Patient(containers.Map({'PatientsName', 'PatientsSex'},{'TestXCAT', 'Male'}));
Stdy = Study(containers.Map({'StudyDescription'},{'Tests reconstruction 3D'}));
% cr�ation de la g�om�trie pour pouvoir adapter le nombre de tranches

% n�cessaires comme pr�cis� ci-dessus
GeoP = GeoP_Factory.create('Siemens2D', 'FFS', 'None');

% Adaptation
nbPixels = floor(32*factMisEch); %% mise � l'�chelle
taillePixels = tailleTranches / nbPixels;
nbTranchesReg = trancheFinReg-trancheIniReg+1;
if ~strcmp(parametres.TrancheUnique,'oui')
    h = (GeoP.Tomographe.RadiusFocus*epaisseurTranches*nbTranchesReg/2)/(GeoP.Tomographe.RadiusFocus-tailleTranches/2);
    longArajouter = h-(nbTranchesReg/2)*epaisseurTranches;
    trancheIni = trancheIniReg-ceil(longArajouter/epaisseurTranches);
    parametres.trancheIni = trancheIni;
    trancheFin = trancheFinReg+ceil(longArajouter/epaisseurTranches);
    parametres.trancheFin = trancheFin;
else
    trancheIni = trancheIniReg;
    trancheFin = trancheFinReg;
    parametres.trancheIni = trancheIni;
    parametres.trancheFin = trancheFin;
end
% Cr�ation du fant�me

MapFant=containers.Map({'Patient', 'Bras','taillePixels', 'nbPixels', 'epaisseurTranches','trancheIni', 'trancheFin'},...
{Pat, Bras,taillePixels, nbPixels, epaisseurTranches,trancheIni, trancheFin});
if strcmp(parametres.Lesion,'oui')
    MapFant('Lesions')=Lesions;
end
Fant = FantomeXCAT(MapFant);
delete MapFant

%% Param�tres du sinogramme XCAT

% R�cup�ration de la source de rayons X
ficsource = fullfile(ROOT,'ct_project_LINUX', 'sp120_5.txt');

TailleObservZ = (trancheFin-trancheIni+1)*epaisseurTranches;
TailleDetecteurZ=(TailleObservZ/GeoP.Tomographe.RadiusFocus)*(GeoP.Tomographe.RadiusFocus + GeoP.Tomographe.RadiusDetector);
nArrays = trancheFin-trancheIni+1; % Autant de detecteurs en Z que de tranches
ArraySize = TailleDetecteurZ/nArrays;

% nArrays =GeoP.nArrays ; % Autant de detecteurs en Z que de tranches
% ArraySize = GeoP.ArraySize ;

nDetectors = nbPixels; %doit �tre pair, mais normalement ca l est tjrs avec la base de 64 pix
nProjections = 2*nDetectors;
AngleTotal = 672*pi/2320; % donn� par Siemens
FanAngleIncrement = AngleTotal/nDetectors;
DetectorOffsetAngle=GeoP.DetectorOffsetAngle;
%DetectorOffsetAngle = 0; %test 
ProjectionAngles = - 2 * pi * (0:nProjections-1) / nProjections;

%%
MapGeoP = containers.Map({'Tomographe', 'nDetectors', 'ArraySize', ...
    'FanAngleIncrement', 'DetectorOffsetAngle', 'ProjectionAngles', 'nArrays'}, ...
    {GeoP.Tomographe, nDetectors, ArraySize, ...
    FanAngleIncrement, DetectorOffsetAngle, ...
    ProjectionAngles, nArrays});
GeoP2 = GeometrieProtocole(MapGeoP);delete(GeoP); delete(MapGeoP); clear MapGeoP;
GeoP = GeoP2;clear GeoP2



ParP = ParP_XCAT(containers.Map({'mAs', 'source','Poisson'},{mAs, struct('Fichier', ficsource),logical(Poisson)}));


TablePosition = - ((trancheIni + trancheFin) / 2 - 1) * epaisseurTranches;
MapSino = containers.Map({'Fant', 'GeoP', 'ParP', 'TablePosition', 'RefAngleDeg'},{Fant, GeoP, ParP,TablePosition, 90});




Sino = Sino_Factory.create('XCAT', 'Map', MapSino,'dir', dir,'nRth',nRth,'nRz',nRz);

delete(MapSino)
clear MapSino
delete(Fant)
clear Fant
delete(GeoP)
clear GeoP
delete(ParP)
clear ParP



return
%%
%% Essais de copie et de destruction
Sino2 = copy(Sino);
Sino.delete

disp(' ')
disp('------------------------------------------------------------------')
disp('Sino d�truit. V�rification de l''int�grit� de Sino2')

Sino2
Sino2.Patient
Sino2.Study
Sino2.GeoP
Sino2.GeoP.Tomographe
Sino2.ParP
Sino2.Fant
Sino2.Fant.Patient
disp('------------------------------------------------------------------')
disp(' ')

Sino2.delete
disp(' ')
disp('------------------------------------------------------------------')
disp('Sino2 d�truit. V�rification de la destruction de tous les objets')
global DEBUGMSG
DEBUGMSG = true;
clear classes
disp('Si aucun autre message n''est apparu, c''est OK')
disp('------------------------------------------------------------------')
