function[J, grad] = polAdPoissSimpvalgrad(f, par)
%
% Fonction calculant la neg-log-vraisemblance avec pénalisation L2 sur 
% un voisinage d'ordre 2 et son gradient en reconstruction tomographique à
% rayons X
%
% Paramètres d'entrée
%  f : valeur courante de l'estimée de l'image reconstruite.
%      Tableau (nRhos*nProjections, 1) 
%  par : structure comportant les champs suivants
%    spar : structure spar
%    Mppol : structure contenant la les éléments pour effectuer les
%            opérations de projection et rétroprojection en coordonnées
%            polaires
%    y : sinogramme sous forme de tableau de taille, de type rpcount
%        (nDetectors*nArrays*nProjections, 1) positif 
%    lambda : valeur du paramètre de régularisation
%    delta : paramètre d'échelle
%    poly : contients tous les paramètres polychromatiques:
%            - source_used : parametres de la source
%            - attmodel : modèles d'attenuation photo et compton
%
%          
% Paramètres de sortie
%   J : critère
%   grad : gradient de J
%

% utiliser avec objet en coordonnees polaires

mae = sqrt(2);
M = par.spar.M;
N = par.spar.N;
Z = par.spar.Z;
Nv = par.y;
Ny = length(Nv(:));
n = length(f(:));

% BRUIT POISSON
% pond_diag = exp(-y(:));
% BRUIT GAUSSIEN

f = reshape(f, M, Z, N);

% Calcul des effets photo et compton

[photo,Jphoto] = par.poly.attmodel.photo(f(:));
[compton,Jcompton] = par.poly.attmodel.compton(f(:));

% calcul des deux projections
p_photo = par.Mppol.Project(photo);
p_compton = par.Mppol.Project(compton);


%calcul de b
b = sum(par.poly.source_used.Intensities(:));
y = log(b./Nv);

% Calcul des vecteurs Somme
    somme1 = par.poly.source_used.Photo*par.poly.I0;
    somme2 = par.poly.source_used.Compton*par.poly.I0;

Somme = y - somme1.*p_photo - somme2.*p_compton;
critAdeq = 1/2*( sum( Nv .* ( Somme ).^2 )); 

% vu que le y ici est de type raysum, on a y = -ln(N/N0) (notations de
% l'exercice 1)
% soit = -ln(y_compte_photon/N0) (notations du calcul du critere)
% d'o� exp(-y) =  y_compte_photons/b (car b = N0)

% total critère

J = critAdeq ;

%% Calcul du gradient

 phiprim = diag(Jphoto);
 thetaprim = diag(Jcompton);


gradAdeq = ( (-somme1.*phiprim - somme2.*thetaprim) .* par.Mppol.RProject(Somme.*Nv)) ;

% total gradient
grad =  gradAdeq;
