classdef polychrom < CritTest
    
    
    properties (SetAccess = protected)
        
        % MPrj - MATRICE DE PROJECTION DU MOD�LE DIRECT (voir d�tails)
        % Objet de la classe "MatriceProjection" ou d'une de ses
        % sous-classes contenant la matrice de projection associ�e au
        % mod�le direct de formation des donn�es.
        par = struct;
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    
    methods
    
        function Adeq = polychrom(Map)
            % CONSTRUCTEUR D'UN OBJET "polychrom"
            % Forme d'appel : Adeq = polychrom(Map)
            % Map : "Map" contenant les �l�ments (cl�s - valeurs)
            % correspondant � toutes les propri�t�s de l'objet
            % "polychrom"
            
            delMap = false;
            if ~ exist('Map', 'var')
                Map = containers.Map();
                delMap = true;
            end
            
            % V�rification de la validit� des param�tres
            [Cles, ~] = findPropsWithDef('polychrom');
            MapComp = polychrom.CheckMap(Map);
            
            % Initialisation des propri�t�s
            params = values(MapComp, Cles);
            for iCle = 1:length(Cles)
                if isa(params{iCle}, 'matlab.mixin.Copyable')
                    Adeq.(Cles{iCle}) = copy(params{iCle});
                else
                    Adeq.(Cles{iCle}) = params{iCle};
                end
            end
            
            delete(MapComp)
            if delMap
                delete(Map)
            end
            
        end
        
        % ------------------------
        
        function vg = val_grad(Adeq, x, y)
            %CALCUL DE LA VALEUR ET DU GRADIENT DE L'�L�MENT DE CRIT�RE
            %   Cette m�thode calcule la valeur et le gradient de
            %   l'�l�ment de crit�re des pour des observations y et une
            %   valeur de l'inconnue x donn�s. Le r�sultat est plac� dans
            %   la sructure vg, qui contient les deux champs "valeur"
            %   et "gradient".
            %   Forme d'appel : vg = val_grad(Adeq, x, y)
            %              ou : vg = Adeq.val_grad(x, y)
%%

%%%%%%%%%%%%%%%%%%%%%%%
% Gros Calcul ci dessous
%%%%%%%%%%%%%%%%%%%%%%%
          
            para = Adeq.par;
            para.y = y;
            
            [valeur, gradient] = poly3D_cg_L2modifieAdapte(x, para);
            

            
             vg = struct('valeur', valeur, ...
                'gradient', gradient);
            
        end

    end
    
   %%
    
    methods (Static)
        
        function MapComp = CheckMap(Map)
            % CheckMap - TESTS SUR LES PARAM�TRES D'ENTR�E DU CONSTRUCTEUR
            % Cette fonction v�rifie que tous les champs n�cessaires sont
            % pr�sents, teste la validit� de ces champs et assigne
            % au besoin des valeurs par defaut
            
            if ~ isa(Map, 'containers.Map')
                error(['Param�tre d''entr�e du constructeur invalide\n', ...
                    'Doit �tre un objet de type "Map"'], [])
            end
            
            [Cles, ValDef] = findPropsWithDef('polychrom');
            MapDef = containers.Map(Cles, ValDef, 'UniformValues', false);
            
            % Compl�tion de Map avec les valeurs par d�faut
            MapComp = [MapDef; Map];
            delete(MapDef)
            
        end
        
    end
    
end

