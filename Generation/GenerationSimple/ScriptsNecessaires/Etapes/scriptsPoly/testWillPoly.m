
%% ESTIMATION DES PARAMETRES DES MODELES PHOTO ET COMPTON

% Matieres considérées pour le calibrage
matnames = {'air',...
            'Al',...
            'blood',... 
            'bone',...
            'fat',... 
            'Fe',...
            'glass',...
            'grayMatter',...
            'Ni',...
            'softTissue',...
            'Ti',...
            'water'...
            };

% Modele photo-electrique
photo_model = @photo_model_vert_hyp;        
photo_plu = [...
             1,-10,10;... % dilate
             1,-10,10;... % hinge
             1,-10,10;... % shift
             1,-10,10 ... % exponent
             ];
photo_weighting = 'equal';

% Modele de Compton
compton_model = @compton_model_horiz_hyp;
compton_plu = [...
              1,-10,10;... % dilate
              1,-10,10;... % hinge
              1,-10,10;... % shift
              2,-10,10 ... % exponent
              ];
compton_weighting = 'equal';

% Energie de reference pour le domaine d'attenuation
E0 = 70;
% Champs des energies considéré
Erange = [20,140];

% Options d'affichage si strictement positif
verbose = 1;
draw_plots = -1;




[p_photo, p_compton, ls_photo, ls_compton, cmp] = photo_compton_fit_model( ...
    photo_model, ...
    photo_plu, ...
    photo_weighting, ...
    compton_model, ...
    compton_plu, ...
    compton_weighting, ...
    matnames, ...
    E0, ...
    Erange, ...
    verbose, ...
    draw_plots ...
    );

% structure polychromatique

poly = struct(...
        'attmodel',[], ...
        'source_used',[], ...
        'I0', [] ...
        );

poly.attmodel = struct(...
                    'photo',@(att0)photo_model(att0,'att0',p_photo),...
                    'compton',@(att0)compton_model(att0,'att0',p_compton)...
                    );

%% SOURCE UTILISEE 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Source complete
if strcmp(parametres.SourceEntiere,'oui')

ficSource='/export/tmp/wdevazel/testWill/Generation/GenerationSimple/DossierSources/sp120_5modifie.txt';
kVp = 120;
poly.source_used = source_spectrum(ficSource,kVp); % utilisation du fichier test à 120 KeV: c'est ici que l'on met la source sous un meilleur format, � savoir purg�e de ses valeurs nulles, mais aussi que l'on cree les coefficients Photo et Compton entre autres ( pour plus d'infos, voir la doc de source remodel) 

% Source reechantillonnee

sp = [poly.source_used.Energies; poly.source_used.Intensities];
source = poly.source_used;
sm.Energies = sp(1,:);
sm.Intensities = 0.5*sp(2,:);
sm.kVp = source.kVp;
sm.RefEnergy = source.RefEnergy;
sm.Photo = (sm.RefEnergy ./ sm.Energies) .^ 3;
sm.Compton = kleinNishina(sm.Energies) ./ kleinNishina(sm.RefEnergy);
poly.source_used = sm;
% poly.I0 = 1;
% poly.source_used.Photo = 1;
% poly.source_used.Compton = 1;    




poly.I0 = poly.source_used.Intensities(:) ./ ...
    sum(poly.source_used.Intensities);
%%%%%%%%%%%%%%%%%%%
else
    
ficSource='/export/tmp/wdevazel/testWill/Generation/GenerationSimple/DossierSources/sp120_5modifie.txt';
kVp = 120;
poly.source_used = source_spectrum(ficSource,kVp); % utilisation du fichier test à 120 KeV

% Source reechantillonnee
nb_ge_samples = parametres.nb_ge_samples;
[ge_model,ge_model_params,ge_model_Err] = ge_model_poly2line(...
                                            source_spectrum(ficSource,kVp),...
                                            73, ... %[30:1:120]
                                            11, ... %[1:30]
                                            [1,2,3]);
%close 1 2 3                               
                                        
poly.source_used = source_remodel(source_spectrum(ficSource,kVp),ge_model,nb_ge_samples);

poly.I0 = poly.source_used.Intensities(:) ./ ...
    sum(poly.source_used.Intensities);
 
plot(poly.source_used.Energies(:),poly.I0(:),'+')

% poly.I0 = 1;
% poly.source_used.Photo = 1;
% poly.source_used.Compton = 1;


end
   