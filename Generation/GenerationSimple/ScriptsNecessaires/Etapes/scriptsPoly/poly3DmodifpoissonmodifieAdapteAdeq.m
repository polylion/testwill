function[J, grad] = poly3DmodifpoissonmodifieAdapteAdeq(f, par)
%
% Fonction calculant la neg-log-vraisemblance avec pénalisation L2 sur 
% un voisinage d'ordre 2 et son gradient en reconstruction tomographique à
% rayons X
%
% Paramètres d'entrée
%  f : valeur courante de l'estimée de l'image reconstruite.
%      Tableau (nRhos*nProjections, 1) 
%  par : structure comportant les champs suivants
%    spar : structure spar
%    Mppol : structure contenant la les éléments pour effectuer les
%            opérations de projection et rétroprojection en coordonnées
%            polaires
%    y : sinogramme sous forme de tableau de taille, de type rpcount
%        (nDetectors*nArrays*nProjections, 1) positif 
%    lambda : valeur du paramètre de régularisation
%    delta : paramètre d'échelle
%    poly : contients tous les paramètres polychromatiques:
%            - source_used : parametres de la source
%            - attmodel : modèles d'attenuation photo et compton
%
%          
% Paramètres de sortie
%   J : critère
%   grad : gradient de J
%

% utiliser avec objet en coordonnees polaires

mae = sqrt(2);
yrp = par.y;
Ny = length(yrp(:));
n = length(f(:));

% BRUIT POISSON
% pond_diag = exp(-y(:));
% BRUIT GAUSSIEN
pond_diag = ones(size(yrp(:)));


% Calcul des effets photo et compton

[photo,Jphoto] = par.poly.attmodel.photo(f(:));
[compton,Jcompton] = par.poly.attmodel.compton(f(:));

% calcul des deux projections
p_photo = par.Mppol.Project(photo);
p_compton = par.Mppol.Project(compton);

% Calcul du vecteur ychap
ychap = zeros(length(p_photo),1);
for i = 1:length(par.poly.I0)
    exp_proj = exp(-(p_photo*par.poly.source_used.Photo(i) + ...
        p_compton*par.poly.source_used.Compton(i)));
    ychap = ychap + exp_proj*par.poly.source_used.Intensities(i)  ;
end

%calcul de b
b = sum(par.poly.source_used.Intensities(:));

ytilde = ychap/b;


critAdeq = sum(ytilde) - yrp'*log(ytilde);

% vu que le y ici est de type raysum, on a y = -ln(N/N0) (notations de
% l'exercice 1)
% soit = -ln(y_compte_photon/N0) (notations du calcul du critere)
% d'o� exp(-y) =  y_compte_photons/b (car b = N0)

% total critère

J = critAdeq ;

%% Calcul du gradient


 e = 1-yrp./ytilde;% attention c'est l� o� il faut prendre en compte le b
 phiprim = diag(Jphoto);
 thetaprim = diag(Jcompton);
 
 
 yphi = 0;
 ytheta = 0;
 for i = 1:length(par.poly.I0)
     phik = par.poly.source_used.Photo(i);
     thetak = par.poly.source_used.Compton(i);
     yphi = yphi + phik*ytilde ;
     ytheta = ytheta + thetak*ytilde;
 end
 
gradAdeq = (- par.Mppol.RProject(yphi.*e).*phiprim -  par.Mppol.RProject(ytheta.*e).*thetaprim);
 

% total gradient
grad =  gradAdeq;
