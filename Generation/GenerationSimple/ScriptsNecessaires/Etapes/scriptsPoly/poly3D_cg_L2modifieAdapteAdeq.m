function[J, grad] = poly3D_cg_L2modifieAdapteAdeq(f, par)
%
% Fonction calculant la neg-log-vraisemblance avec pénalisation L2 sur 
% un voisinage d'ordre 2 et son gradient en reconstruction tomographique à
% rayons X
%
% Paramètres d'entrée
%  f : valeur courante de l'estimée de l'image reconstruite.
%      Tableau (nRhos*nProjections, 1) 
%  par : structure comportant les champs suivants
%    spar : structure spar
%    Mppol : structure contenant la les éléments pour effectuer les
%            opérations de projection et rétroprojection en coordonnées
%            polaires
%    y : sinogramme sous forme de tableau de taille
%        (nDetectors*nArrays*nProjections, 1) positif 
%    lambda : valeur du paramètre de régularisation
%    delta : paramètre d'échelle
%    poly : contients tous les paramètres polychromatiques:
%            - source_used : parametres de la source
%            - attmodel : modèles d'attenuation photo et compton
%
%          
% Paramètres de sortie
%   J : critère
%   grad : gradient de J
%

% utiliser avec objet en coordonnees polaires

mae = sqrt(2);

M = par.spar.M;
N = par.spar.N;
Z = par.spar.Z;
y = par.y;


% BRUIT POISSON
% pond_diag = exp(-y(:));
% BRUIT GAUSSIEN
pond_diag = ones(size(y(:)));

f = reshape(f, M, Z, N);

% Calcul des effets photo et compton

[photo,Jphoto] = par.poly.attmodel.photo(f(:));
[compton,Jcompton] = par.poly.attmodel.compton(f(:));

p_photo = par.Mppol.Project(photo);
p_compton = par.Mppol.Project(compton);


sino = zeros(length(p_photo),3);

for i = 1:length(par.poly.I0)
    exp_proj = exp(-(p_photo(:)*par.poly.source_used.Photo(i) + ...
        p_compton*par.poly.source_used.Compton(i)));
    sino = [sino(:,1) + exp_proj*par.poly.I0(i), ...
            sino(:,2) + exp_proj*par.poly.I0(i)*par.poly.source_used.Photo(i), ...
            sino(:,3) + exp_proj*par.poly.I0(i)*par.poly.source_used.Compton(i)];
end




PROJ = 1;
PHOTO = 2;
COMPTON = 3;


%% Calcul du critère
  
residual = y(:) + log(sino(:,PROJ));

% disp(['residual: ',num2str(mean(mean(residual)) )]) 

pondresidual = pond_diag .* residual;


% total critère
J = residual' * pondresidual ;

%% Calcul du gradient

quot_residual = residual./sino(:,PROJ);

Gphoto = sino(:,PHOTO) .* quot_residual;
Gcompton = sino(:,COMPTON) .* quot_residual;
%%% a changer ceci est un test
grad_photo = par.Mppol.RProject(Gphoto);
grad_compton = par.Mppol.RProject(Gcompton);




% total gradient
grad = - (Jphoto * grad_photo + Jcompton * grad_compton);
