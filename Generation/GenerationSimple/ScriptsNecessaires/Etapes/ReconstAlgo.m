% ReconsAlgo
% choix des param�tres
if (isa(parametres.imageInitialeVal, 'float') && length(parametres.imageInitialeVal) == 1 )
    x0 = parametres.imageInitialeVal*ones(size(serieW.Val));
    'image initialisee uniformement'
else  x0 = parametres.imageInitialeVal;
end
verbosity = 1;
nb_corr = 10;
ftol = 0;
max_rt = 1e+10;
upb = inf;
ftol = 0;

algoPars = struct('x0', x0, ...
    'lowb', lowb, ...
    'upb', upb, ...
    'ftol', ftol, ...
    'epsilon', epsilon, ...
    'nb_corr', nb_corr, ...
    'max_iter', max_iter, ...
    'max_rt', max_rt, ...
    'max_fg', 3 * max_iter, ...
    'verbosity', verbosity);

% Choix de la m�thode de reconstruction

idxScanW=1;

% Si l'on a choisi la FBP pour initialiser
if strcmp(parametres.ImageInitiale,'FBP') || strcmp(parametres.ImageInitiale,'posFBP')
    if strcmp(Sino.Type,'RPCount')
        Sino2 = Sino.cvRaySum();
    else 
        Sino2 =Sino;
    end
    recMIni = FBP(containers.Map({'GeoS','Sino','idxScan','MatP'},{geoSW,Sino2,idxScanW,mpW}));
    [SrieIni,recInfo1] = reconst(recMIni);
    
    algoPars.x0 = SrieIni.Val;
    if strcmp(parametres.ImageInitiale,'posFBP')  
    algoPars.x0(find(algoPars.x0<0)) = 0;    
    end
    delete Sino2
end







if (strcmp(parametres.CritereAdeq,'Linear_Gauss_noPond') || strcmp(parametres.CritereAdeq,'Exponential_Poisson')|| strcmp(parametres.CritereAdeq,'polychromAdeqPoisson') || strcmp(parametres.CritereAdeq,'polAdPoissSimp'))  
    reconstrucW = minCrit(containers.Map({'GeoS','Sino','crit','idxScan', 'algo','algoPars','precond'},{geoSW,Sino,critW,idxScanW,@lbfgsb,algoPars,precW}));
else 
    reconstrucW = minCrit(containers.Map({'GeoS','Sino','crit','idxScan', 'algo','algoPars'},{geoSW,Sino,critW,idxScanW,@lbfgsb,algoPars}));
end