%CritGeneral


% ici on a choisi le non Poissonnien

if (strcmp(CritereAdeq, 'polychromAdeq') ||strcmp(CritereAdeq, 'polychromAdeqPoisson') || strcmp(parametres.CritereAdeq,'polAdPoissSimp'))
    eval(strcat('adeqW = ',CritereAdeq,'(containers.Map({''par'',''MPrj''},{par,mpW}))     '));
elseif strcmp(CritereAdeq, 'Linear_Gauss_noPond') || strcmp(CritereAdeq, 'Exponential_Poisson')|| strcmp(CritereAdeq, 'Exponential_PoissonRS')
    eval(strcat('adeqW = ',CritereAdeq,'(containers.Map({''MPrj''}, {mpW}))   '));
end


if (strcmp(CriterePenal, 'PenalGradObj_L2L1') || strcmp(CriterePenal, 'PenalObj_L2L1') )
    eval(strcat('penalW= ',CriterePenal,'(containers.Map({''GeoS'',''delta'',''lambda''},{geoSW,delta,lambda}))' ));
elseif (strcmp(CriterePenal, 'PenalObj_L2') || strcmp(CriterePenal, 'PenalGradObj_L2') )
    eval(strcat('penalW= ',CriterePenal,'(containers.Map({''GeoS'',''lambda''},{geoSW,lambda}))' ));
end


nElemtsW=2;
critW = Critere(containers.Map({'J'},{{adeqW,penalW}}));
