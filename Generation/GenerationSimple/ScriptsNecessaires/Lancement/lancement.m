names = fieldnames(parametres);

for i = 1:length(names)
key = char(names(i));
eval(strcat(key,' = parametres.(key)') );
end

GenFantEtSino % j ai du mettre parametres pour recuperer les tanches ini et reg pour le trace aes

Initialisation


if strcmp(parametres.Geom,'Cartesien')
    MatprojGenCart
elseif strcmp(parametres.Geom,'Polaire')
    nThetas = Sino.GeoP.nProjections;
    nRhos = floor((Sino.Fant.nbPixels)^2/nThetas) ;
    serieW = serieW.cvPol(nRhos,nThetas);
    MatprojGenCyl
end

if (strcmp(parametres.CritereAdeq,'polychromAdeq') || strcmp(parametres.CritereAdeq,'polychromAdeqPoisson') || strcmp(parametres.CritereAdeq,'polAdPoissSimp') )  
    initipoly
end

CritGen

if (strcmp(parametres.CritereAdeq,'Linear_Gauss_noPond') || strcmp(parametres.CritereAdeq,'Exponential_Poisson') || strcmp(parametres.CritereAdeq,'polychromAdeqPoisson') || strcmp(parametres.CritereAdeq,'polAdPoissSimp'))  
    PrecondGen
end

ReconstAlgo % la dedans il dit que si poly alors pas de precond
[Sriefinal, recInfo]=reconst(reconstrucW);


% save 
%%

directory = '/export/tmp/wdevazel/testWill/Generation/GenerationSimple/DossierResultats/';
giveName
% save( strcat(directory,name,'.mat'), 'parametres', 'Sriefinal', 'recInfo');
