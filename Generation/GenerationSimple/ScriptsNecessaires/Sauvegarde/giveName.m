if (strcmp(parametres.CritereAdeq,'polychromAdeq') || strcmp(parametres.CritereAdeq,'polychromAdeqPoisson')) 
Polychro = 'poly';
else
Polychro = 'mono';
end

if strcmp(parametres.Geom,'Cartesien')
    Geom = 'Cart';
elseif strcmp(parametres.Geom,'Polaire')
    Geom = 'Pol';
end
 
nb = parametres.epsilon;
precision = 2; % nombre de chiffres significatifs, doit etre superieur ou egal a 2, recommande:3

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%petit programme pour noter dans le fichier la precision
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
precision = precision-1;
a = num2str(nb,strcat('%.',int2str(precision),'e'));
l = length(a);
fin = str2double(a(4+precision:l))-precision;
a = strcat(a(1:1),a(3:3+precision),num2str(fin));


c = clock;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% il faut aussi 

name = strcat(Polychro, Geom, a,'_', date,'_',num2str(c(4)),'-',num2str(c(5)) );