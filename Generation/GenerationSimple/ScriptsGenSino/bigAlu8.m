% Ceci est un fichier pour rentrer facilement les parametres des fantomes a
% reconstruire, avec les lesions
% ce fichier est a copier, a modifier, et a coller dans le bon dossier


% Tranche Initiale et tranche finale : si on veut uniquement des lesions,
% faire autour de z = 140 je crois

parametres.factMisEch = 5   ;
parametres.Bras              =   0   ;        % pr�sence ou non d'un bras
parametres.epaisseurTranches =   1   ;        % mm
parametres.trancheIniReg        =   3000 ;     % tranches qu'on voudra reconstruire
parametres.trancheFinReg        =   3000 ;
parametres.TrancheUnique='oui';
parametres.nRth = 8;
parametres.nRz = 1;



% precision si oui ou non il y a des lesions

parametres.Lesion = 'oui';


% creation de l eau comme milieu
   
    
    z  = 3000;
    materials = 15; % brain ca doit etre mou
    proportions = 1;

    diam = 400;
    
    x = 0;
    y = 0;
    Lesion5 = LesionXCAT(containers.Map({'x','y','z','diam','materials','proportions'},...
        {x,y,z,diam,materials,proportions}));

 %Creation des lesions metalliques
    
    z  = 3000;
    materials = 20; % aluminium

    diam = 100;
    
    x = 0;
    y = 0;
    
    Lesion1 = LesionXCAT(containers.Map({'x','y','z','diam','materials','proportions'},...
        {x,y,z,diam,materials,proportions}));
    
  
   
    
    %%%%%%%%%%%%%%%%%%%%
    Lesions = [Lesion5 Lesion1];


