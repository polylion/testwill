% Ceci est un fichier pour rentrer facilement les parametres des fantomes a
% reconstruire, avec les lesions
% ce fichier est a copier, a modifier, et a coller dans le bon dossier


% Tranche Initiale et tranche finale : si on veut uniquement des lesions,
% faire autour de z = 140 je crois

parametres.factMisEch = 5   ;
parametres.Bras              =   0   ;        % pr�sence ou non d'un bras
parametres.epaisseurTranches =   1   ;        % mm
parametres.trancheIniReg        =   901 ;     % tranches qu'on voudra reconstruire
parametres.trancheFinReg        =   901 ;

parametres.TrancheUnique = 'oui';



% precision si oui ou non il y a des lesions

parametres.Lesion = 'oui';

 %Creation de la lesion metallique
    x = 100;
    y = 75;
    z  = 900;
    diam = 21;
    materials = 20; % melange de titane et d'aluminium
    proportions = 1;
    
    
    Lesion1 = LesionXCAT(containers.Map({'x','y','z','diam','materials','proportions'},...
        {x,y,z,diam,materials,proportions}));
    
    
  %Creation de la lesion metallique
    x = 25;
    y = 0;
    z  = 900;
    diam = 10;
    materials = 20; % melange de titane et d'aluminium
    proportions = 1;
    
    
    Lesion2 = LesionXCAT(containers.Map({'x','y','z','diam','materials','proportions'},...
        {x,y,z,diam,materials,proportions}));   
    
    
    Lesions = [Lesion1 Lesion2];


