
% Ceci est un fichier pour rentrer facilement les parametres des fantomes a
% reconstruire, avec les lesions
% ce fichier est a copier, a modifier, et a coller dans le bon dossier


% Tranche Initiale et tranche finale : si on veut uniquement des lesions,
% faire autour de z = 140 je crois

parametres.factMisEch = 5   ;
parametres.Bras              =   0   ;        % pr�sence ou non d'un bras
parametres.epaisseurTranches =   1   ;        % mm
parametres.trancheIniReg        =   1077 ;     % tranches qu'on voudra reconstruire
parametres.trancheFinReg        =   1077 ;
parametres.TrancheUnique='oui';


parametres.nRth = 1;
parametres.nRz = 1;

parametres.Poisson = 1; % 1 pour rajouter du bruit de Poisson
parametres.mAs = 0.0043 ;

% precision si oui ou non il y a des lesions

parametres.Lesion = 'non';
